import React, { useState } from 'react';
import axios from 'axios';

import { sizes } from '../shared/sharedVariables';

const UploadForm = () => {
    const maxFileSize = 100 * sizes.KB;
    const allowedExt = ['jpeg', 'jpg'];

    const [file, setFile] = useState(null);

    const handleFile = (e) => {
        const fileInfo = e.target.files[0];
        setFile(fileInfo);
    }

    const submitUpload = () => {
        const file_ext = file.name.substr(file.name.lastIndexOf(".") + 1);
        const found = allowedExt.find(ext => ext === file_ext);
        if(found === undefined) return console.error("File extention is not suppoerted");

        const fileSize = file.size;
        if(fileSize > maxFileSize) return console.error("File size is too large");

        //file size and extention are limited on the backend as well
        
        uploadToServer('http://localhost:3001/file', 'file', file);
    }

    const uploadToServer = (serverUrl, fieldName, fileToUpload) => {
        let formdata = new FormData()
        formdata.append(fieldName, fileToUpload)
        //formdata.append('name', 'test image') //Find out what is it for

        axios({
            url: serverUrl,
            method: 'POST',
            header: {
                authorization: ''
            },
            data: formdata
        })
        .then((res) => console.log(res.data))
        .catch((err) => console.log(err))
    }

    return (
        <div>
            <label>Example file input</label><br />
            <input type="file" name ='file' className="form-control-file" onChange={(e)=> handleFile(e)}></input>
            <br /> <br />
            <button type="button" className="btn btn-primary" onClick={submitUpload}>Upload</button>
        </div>
    )
}

export default UploadForm;