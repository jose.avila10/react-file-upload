import React from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import UploadForm from './Components/UploadForm';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/upload" element={<UploadForm />} />
      </Routes>
    </Router>
  );

}

export default App;
