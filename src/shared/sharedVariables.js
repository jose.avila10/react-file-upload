export const sizes = {
    "KB": Math.pow(1024, 1),
    "MB": Math.pow(1024, 2),
    "GB": Math.pow(1024, 3),
    "TB": Math.pow(1024, 4),
}